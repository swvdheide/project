//
//  journalItem.swift
//  bookkeeping
//
//  Created by Sven van der Heide on 17-03-16.
//  Copyright © 2016 Sven van der Heide. All rights reserved.
//

import ObjectMapper
import Cocoa

class JournalEntry: NSObject, Mappable{
    var name:String
    var date:Date
    var repeatable:Bool?
    var id:String
    
    
    fileprivate(set) var journalEntryComponents:[JournalEntryComponent]?
    
    
    init?(newName:String, newDate:Date, newJournalEntryComponents:[JournalEntryComponent]) {
        
        if(checkBalanceComponents(newJournalEntryComponents)){
            name = newName
            date = newDate
            id = UUID().uuidString
            super.init()
            journalEntryComponents = newJournalEntryComponents
            journalEntryList.append(self)
            self.addJournalEntryComponentsToBalanceItems()
            self.addJournalEntrytoComponents()
            saveJournalEntry( journalEntries: "[" + Mapper().toJSONString(self, prettyPrint: true)! + "]")
            
            print("created a new journal entry with name", newName)
            
        }else{
            print("failed to create journal entry with name: ", newName)
            return nil
        }
    }
    required init?(map: Map){
        name = "hoi"
        date = NSDate() as Date
        id = NSUUID().uuidString
        super.init()
        self.mapping(map: map)
        print(self.journalEntryComponents )
        for jec in journalEntryComponents ?? [JournalEntryComponent](){
            jec.journalEntry = self
            jec.addToBalanceItem()
            print("bi name",jec.balanceItem.name)
        }
        journalEntryList.append(self)
    }
    
    func replaceJournalEntry(_ newName:String, newDate:Date, newJournalEntryComponents:[JournalEntryComponent]){
        if(checkBalanceComponents(newJournalEntryComponents)){
            name = newName
            date = newDate
            self.replaceJournalEntryComponents(newJournalEntryComponents)
            saveJournalEntry( journalEntries: "[" + Mapper().toJSONString(self, prettyPrint: true)! + "]")
            print("created a new journal entry with name", newName)
            
        }else{
            print("failed to create journal entry with name: ", newName)
        }
    }
    
    func replaceJournalEntryComponents(_ newJournalEntryComponents:[JournalEntryComponent]){
        self.deleteJournalEntryComponentsToBalanceItems()
        self.journalEntryComponents = newJournalEntryComponents
        self.addJournalEntrytoComponents()
        self.addJournalEntryComponentsToBalanceItems()
    }
    
    func deleteJournalEntry(){
        if let index = journalEntryList.index(of: self){
            journalEntryList.remove(at: index)
        }
        self.deleteJournalEntryComponentsToBalanceItems()
        
    }
    
    fileprivate func addJournalEntrytoComponents(){
        for component in journalEntryComponents!{
            component.journalEntry = self

            //saveJECToDB(component)
        }
    }
    
    
    fileprivate func addJournalEntryComponentsToBalanceItems(){
        for component in journalEntryComponents!{
            component.addToBalanceItem()
            
            
        }
    }
    

    
    

    
    
    fileprivate func deleteJournalEntryComponentsToBalanceItems(){
        for component in journalEntryComponents!{
            component.deleteFromBalanceItem()
        }
    }
    
    func mapping(map: Map) {
        name    <- map["Name"]
        date    <- (map["Date"], DateTransform())
        id      <- map["ID"]
        journalEntryComponents <- map["journalEntryComponents"]

    }

    
    
    
}

extension JournalEntry{
    
}

//func GetFIRObjects(fbo:FireBaseObject)->[JournalEntry]{
//    let ref = FIRDatabase.database().reference()
//    var journalEntryListTemp = [JournalEntry]()
//    ref.child(fbo.className).observeSingleEventOfType(.Value, withBlock: { (snapshot) in
//        // Get user value
//        for object in snapshot.children{
//            journalEntryListTemp.append(JournalEntry(FirInit: object as! FIRDataSnapshot))
//        }
//
//        // ...
//    }) { (error) in
//        print(error.localizedDescription)
//    }
//    return journalEntryListTemp
//}

func saveJournalEntry(journalEntries : String){
    
    let request = NSMutableURLRequest(url: URL(string: "http://ec2-52-59-128-204.eu-central-1.compute.amazonaws.com/SamplePage.php")!)
    
    let entry = "content=\(journalEntries)"
    print("ivo is een held",entry)
    
    request.httpMethod = "POST"
    request.httpBody = entry.data(using: String.Encoding.utf8)
    //print("simple test request body ",request.httpBody)
    
    let task = URLSession.shared.dataTask(with: request as URLRequest) {
        data, response, error in
        
        if error != nil {
            print("error=\(error)")
            return
        }
        
        print("response = \(response)")
        
        let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
        print("responseString = \(responseString)")
    }
    task.resume()
}


func checkBalanceComponents(_ journalEntryComponents:[JournalEntryComponent])->Bool{
    var totalValue:Float = 0
    for component  in journalEntryComponents{
        totalValue += component.isDebet ? component.amount : component.amount * -1
    }
    if(journalEntryComponents.count > 1 && Int(round(1000 * totalValue)) == 0){
        return true
    }else{
        print("balance item not in balance" , totalValue)
        return false
    }
}
func getJournalEntries(){
    let urlRequest = NSMutableURLRequest(url: URL(string: "http://ec2-52-59-128-204.eu-central-1.compute.amazonaws.com/SamplePage.php")!)
    
    let task = URLSession.shared.dataTask(with: urlRequest as URLRequest) {
        (data, response, error) -> Void in
        
        let httpResponse = response as! HTTPURLResponse
        let statusCode = httpResponse.statusCode
        if (statusCode == 200) {
            print("Everyone is fine, file downloaded successfully.",data)
            
            do{
                
                let json = try JSONSerialization.jsonObject(with: data!, options:[])
                print(json)
                for entry in json as! [[String: AnyObject]]{
                    
                    let je = JournalEntry(map: Map(mappingType: .fromJSON , JSON: entry))
                    
                    print("sven hoi",  je?.date)
                }
            }catch {
                print("Error with Json: \(error)")
            }
            
            
        }
    }
    task.resume()
}

//func seperate
//
//func checkBalanceJournalItemComponents(journalItemComponents:[JournalItemComponent]){
//    
//    
//}
