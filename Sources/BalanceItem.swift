//
//  BalanceItem.swift
//  bookkeeping
//
//  Created by Sven van der Heide on 23-03-16.
//  Copyright © 2016 Sven van der Heide. All rights reserved.
//

import ObjectMapper
import Cocoa

let realUrl = "http://ec2-52-59-128-204.eu-central-1.compute.amazonaws.com/"
let testUrl = "http://127.0.0.1:8181/"
let testurl3 = "http://motherfuckingwebsite.com"
let testUrl2 = "http://ec2-52-57-194-158.eu-central-1.compute.amazonaws.com:8181"
let requesturl = testUrl


class BalanceItem: NSObject, Mappable {
    var isDebet:Bool = false
    var name:String = "hoi"
    var id:String = UUID().uuidString
    var journalEntryComponents = [JournalEntryComponent]()
    var isProfitLoss:Bool?
    var mainBalanceItem:BalanceItem?
    var relatedBalanceItem = [BalanceItem]()
    
    
    
    //PickerDelegate<T:InputProtocol, Delegate:InputViewDelegate where Delegate.T == T>
        
        //= PickerDelegate<BalanceItem, U>(delegate: inputOptions)

    
    init(mainBalanceItem newIsDebet:Bool, newName:String){
        isDebet = newIsDebet
        name = newName
        id = UUID().uuidString
        super.init()
        print("json",(Mapper().toJSONString(self, prettyPrint: false)!))
        let content = "[" + Mapper().toJSONString(self, prettyPrint: false)! + "]"
        saveBalanceItem(content: content)
        balanceItemList.append(self)
        
    }
    
    required init?(map: Map) {
//        self.isDebet = true
//        self.name = "test"
//        id = UUID().uuidString
        super.init()
//        
        self.mapping(map: map)
        balanceItemList.append(self)
        
    }
    
    init(subBalanceItem newIsDebet:Bool, newName:String, mainBalanceItem:BalanceItem){
        isDebet = newIsDebet
        name = newName
        id = UUID().uuidString
        super.init()
        mainBalanceItem.relatedBalanceItem.append(self)
        self.mainBalanceItem = mainBalanceItem
        
        print("created new sub balance Item: ", newName)
        
        
    }
    
    func addJournalEntryComponent(_ journalEntryComponent:JournalEntryComponent) -> Bool{
        journalEntryComponents.append(journalEntryComponent)
        return true
    }
    
    func deleteJournalEntryComponent(_ journalEntryComponent:JournalEntryComponent) -> Bool{
        if let index =  journalEntryComponents.index(of: journalEntryComponent){
            journalEntryComponents.remove(at: index)
            return true
        }else{
            print("deleteJournalEntryComponent | could not find: " +  journalEntryComponent.journalEntry!.name + journalEntryComponent.balanceItem.name)
            return false
        }
    }
    
    func getAmountForDate(_ date:Date)->Float{
        var totalAmount:Float = 0
        let relevantComponents = self.journalEntryComponents.filter { (comp) -> Bool in
            (comp.journalEntry?.date.isLessThanDate(date))!
        }
        for component in relevantComponents{
            totalAmount += component.isDebet == self.isDebet ? component.amount : component.amount * -1;
        }
        return totalAmount
    }
    
    func getTotalAmountForDate(_ date:Date)->Float{
        var totalAmount:Float = 0
        var relevantComponents = self.journalEntryComponents
        for bi in self.relatedBalanceItem{
            relevantComponents += bi.journalEntryComponents
        }
        
        relevantComponents = relevantComponents.filter { (comp) -> Bool in
            (comp.journalEntry?.date.isLessThanDate(date))!
        }
        for component in relevantComponents{
            totalAmount += component.isDebet == self.isDebet ? component.amount : component.amount * -1;
        }
        return totalAmount
    }
    func getRelatedItemList()->[BalanceItem]{
        var relatedItems = relatedBalanceItem
        relatedItems.append(self)
        return relatedItems
    }
    
    
    func getAmountForPeriod(_ date:[Date])->[Float]?{
        var totalPositveAmountAmount:Float = 0
        var totalNegativeAmount:Float = 0
        guard date.count == 2 else{
            return nil}
        
        var dateS = date.sorted { (date1, date2) -> Bool in
            date1.isLessThanDate(date2)
        }
        
        let componentsFiltered = self.journalEntryComponents.filter { (comp) -> Bool in
            let firstStatement = comp.journalEntry!.date.isGreaterThanDate(dateS[0])
            let secondStatement = comp.journalEntry!.date.isLessThanDate(dateS[1])
            return firstStatement && secondStatement
        }

        for component in componentsFiltered{
            totalPositveAmountAmount += component.isDebet == self.isDebet ? component.amount : 0.0
            totalNegativeAmount += component.isDebet != self.isDebet ? component.amount : 0.0
            
        }
        
        
        
        return [totalPositveAmountAmount,totalNegativeAmount]
    }
    
    func mapping(map: Map) {
        name    <- map["Name"]
        isDebet    <- (map["IsDebit"], BooleanTransform())
        id      <- map["ID"]
        mainBalanceItem <- map["parentBIID"]
    }
    func getWriteStatemnt()->String{
        return "INSERT INTO `perfectdb`.`BalanceItem` (`ID`, `Name`, `IsDebit`, `UID`) VALUES ('\(id)', '\(name)', \(isDebet), 123213);"
    }
    
   
    
    
}


func getBalanceItemResponse(response: HTTPURLResponse, data:Data){
    let statusCode = response.statusCode
    
    if (statusCode == 200) {
        print("Everyone is fine, file downloaded successfully.",data)
        
        do{
            
            let json = try JSONSerialization.jsonObject(with: data, options:[])
            print(json)
            for entry in json as! [[String: AnyObject]]{
                
                let bi = BalanceItem(map: Map(mappingType: .fromJSON , JSON: entry))
                
                print("sven hoi",  bi?.isDebet)
            }
            getJournalEntries()
        }catch {
            print("Error with Json: \(error)")
        }
        
    }
}
func getBalanceItems(){
    let urlRequest = NSMutableURLRequest(url: URL(string: "\(realUrl)getBalanceItems.php")!) //getBalanceItems.php"
    let task = URLSession.shared.dataTask(with: urlRequest as URLRequest) {
        (data, response, error) -> Void in
        getBalanceItemResponse(response: response as! HTTPURLResponse, data: data!)
        
    }
    task.resume()
    
}
class BooleanTransform: TransformType{
    typealias Object = Bool
    typealias JSON = String
    func transformFromJSON(_ value: Any?) -> Object?{

        return (value as? String  == "1") ? true : false;
    }
    
    func transformToJSON(_ value: Object?) -> JSON?{
        return value! ? "1"  : "0";
    }
}

class BalanceItemTransform: TransformType{
    typealias Object = BalanceItem
    typealias JSON = String
    func transformFromJSON(_ value: Any?) -> Object?{
        let index = balanceItemList.index { (bi) -> Bool in
            bi.id == value as? String
        }
        return index != nil ? balanceItemList[index!] : nil
    }
    
    func transformToJSON(_ value: Object?) -> JSON?{
        return value?.id
    }
}

func saveBalanceItemPerfect(content:String){
    //let urlRequest = NSMutableURLRequest(url: URL(string: "\(requesturl)balanceItem")!) //getBalanceItems.php"
    let request = NSMutableURLRequest(url: URL(string: "\(requesturl)balanceItem")!)//"addBalanceItem.php"
    let entry = "content=\(content)"
    request.httpMethod = "POST"
    let postString = entry
    request.httpBody = postString.data(using: String.Encoding.utf8)
    print("simple test request",request)
    
    let task = URLSession.shared.dataTask(with: request as URLRequest) {
        data, response, error in
        
        if error != nil {
            print("error=\(error)")
            return
        }
        
        print("response = \(response)")
        
        let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
        print("responseString = \(responseString)")
        
    }
    task.resume()
//    let task = URLSession.shared.dataTask(with: urlRequest as URLRequest) {
//        (data, response, error) -> Void in
//        
//        let httpResponse = response as! HTTPURLResponse
//        let statusCode = httpResponse.statusCode
//        
//        if (statusCode == 200) {
//            print("Everyone is fine, file downloaded successfully.",data)
//            
//            do{
//                
//                let json = try JSONSerialization.jsonObject(with: data!, options:[])
//                print(json)
//                for entry in json as! [[String: AnyObject]]{
//                    
//                    let bi = BalanceItem(map: Map(mappingType: .fromJSON , JSON: entry))
//                    
//                    print("sven hoi",  bi?.isDebet)
//                }
//                getJournalEntries()
//            }catch {
//                print("Error with Json: \(error)")
//            }
//            
//        }
//    }
//    task.resume()
    
}
func saveBalanceItem(content:String){
    saveBalanceItemPerfect(content: content)
    //    let request = NSMutableURLRequest(url: URL(string: requesturl)!)//"addBalanceItem.php"
    //    let entry = "content=\(content)"
    //    request.httpMethod = "GET"
    //    let postString = entry
    //    request.httpBody = postString.data(using: String.Encoding.utf8)
    //    print("simple test request",request)
    //
    //    let task = URLSession.shared.dataTask(with: request as URLRequest) {
    //        data, response, error in
    //
    //        if error != nil {
    //            print("error=\(error)")
    //            return
    //        }
    //
    //        print("response = \(response)")
    //
    //        let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
    //        print("responseString = \(responseString)")
    //
    //    }
    //    task.resume()
    
}



func splitDebitAndCreditBalanceItems(_ balanceItems:[BalanceItem])->[[BalanceItem]]{
    let debitBI = balanceItems.filter { (bi) -> Bool in
        bi.isDebet
    }
    let crebitBI = balanceItems.filter { (bi) -> Bool in
        !bi.isDebet
    }
    
    return [debitBI, crebitBI]
}

var dummyBalanceItemList = [""]
var balanceItemList = [BalanceItem]()
var journalEntryList = [JournalEntry]()

