import PackageDescription
let package = Package(
    name: "project",
    targets: [],
    dependencies: [

        .Package(url: "https://github.com/Hearst-DD/ObjectMapper.git", majorVersion: 2, minor: 2)
        
    ]
)
